package entitati;

import java.io.Serializable;

public abstract class Account implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -757551100888844086L;
	/**
	 * 
	 */

	private String type;
	private float suma;
	private Valuta valuta;
	private long cod;
	private Client client;
	
	
	public Account(String type, float suma, Valuta valuta, long cod, Client client) {
		this.type = type;
		this.suma = suma;
		this.valuta = valuta;
		this.cod = cod;
		this.client = client;
	}
	
	public boolean retrageBani(float suma) {
		/*float sumaCurenta = this.getSuma();
		float sumaNoua= sumaCurenta-suma;
		this.setSuma(sumaNoua);*/
		return true;
	}

	public boolean depuneBani(float suma, double nrLuni) {
		/*float sumaCurenta=this.getSuma();
		float sumaDobanda= (float) (suma*nrLuni*dobanda);
		float sumaNoua=suma+sumaCurenta+ sumaDobanda;
		this.setSuma(sumaNoua);*/
		return true;
	}
	
	/*public Account() {
		
	}*/
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the sum
	 */
	public float getSuma() {
		return suma;
	}
	/**
	 * @param sum the sum to set
	 */
	public void setSuma(float suma) {
		this.suma = suma;
	}
	/**
	 * @return the valuta
	 */
	public Valuta getValuta() {
		return valuta;
	}
	/**
	 * @param valuta the valuta to set
	 */
	public void setValuta(Valuta valuta) {
		this.valuta = valuta;
	}
	/**
	 * @return the cod
	 */
	public long getCod() {
		return cod;
	}
	/**
	 * @param cod the cod to set
	 */
	public void setCod(long cod) {
		this.cod = cod;
	}
	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}
	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}
	
	
	

}
