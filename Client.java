package entitati;
import java.io.Serializable;

public class Client implements Serializable{
	
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8046636185037843429L;
	private long cnp;
	private String nume;
	private String prenume;	
	private String email;
	
	
	
	public Client(String nume, String prenume, long cnp, String email) {
		this.nume = nume;
		this.prenume = prenume;
		this.cnp = cnp;
		this.email = email;
	}
	
	public Client(String nume, String prenume, String email) {
		this.nume = nume;
		this.prenume = prenume;
		this.email = email;
	}
	public Client() {
		
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public long getCnp() {
		return cnp;
	}
	public void setCnp(long cnp) {
		this.cnp = cnp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [cnp=" + cnp + ", nume=" + nume + ", prenume=" + prenume + ", email=" + email + "]";
	}
	
	
	

}
