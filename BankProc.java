package bank;

import java.util.ArrayList;
import java.util.Hashtable;

import entitati.Account;

public interface BankProc {
	
	public boolean adaugaCont(Account account,long clientCNP);

	public boolean stergeCont(Account account, long clientCNP);

	public void editeazaCont(Account account, long clientCNP);
	
	public void scrieInfisier();
	
	public Hashtable<Long, ArrayList<Account>> citesteDinFisier();
		

}
