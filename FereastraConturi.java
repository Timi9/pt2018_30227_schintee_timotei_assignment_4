package presentation;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import bank.Banc;
import entitati.*;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.awt.event.ActionEvent;

public class FereastraConturi  {
	private static final long serialVersionUID = 1L;


	private JFrame frame;
	private JTable table;

	private Object[][] data = { { "Ra", "23332", "3232323", "33", "Spending", "EURO" } };
	
	private String[] column = { "NumeClient", "CNP", "CodIBAN", "Suma", "TipCont", "Valuta" };

	private Banc banc = new Banc();
	private JLabel lblAdaugaContNou;
	private JLabel lblCodiban;
	private JLabel lblSuma;
	private JLabel lblTipcont;
	private JLabel lblValuta;
	private JLabel lblronLiraEuro;
	private JTextField textField_IBAN;
	private JTextField textField_suma;
	private JTextField textField_tipCont;
	private JTextField textField_valuta;
	private JButton btnCreazaCont;
	private JLabel lblCnpclient;
	private JTextField textField_cnp;
	private JButton btnRetrageBani;
	private JLabel lblSuma_1;
	private JLabel lblCnp;
	private JTextField textField_suma_retrage;
	private JTextField textField_cnp_retrage;
	private JButton btnDepuneBani;
	private JTextField textField_luniDobanda;
	private JLabel lblIban;
	private JTextField IBANFIELD;
	private JButton btnStergecont;
	private JLabel lblCnpclient_1;
	private JTextField cnp_stergeCont;
	private JTextField textField_stergereType;
	private JLabel lblType;

	public FereastraConturi() {
		initialize();
		frame.setVisible(true);
		
	}

	private void initialize() {
		frame = new JFrame();
		table = new JTable(new DefaultTableModel(data, column));
		frame.setTitle("Administrare Conturi");
		frame.setBounds(100, 100, 829, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(329, 36, 458, 391);
		frame.getContentPane().add(scrollPane);

		nouTabel("Merge");
		scrollPane.setColumnHeaderView(table);
		scrollPane.setViewportView(table);

		lblAdaugaContNou = new JLabel("Adauga cont nou");
		lblAdaugaContNou.setBounds(32, 43, 126, 23);
		frame.getContentPane().add(lblAdaugaContNou);

		lblCodiban = new JLabel("CodIBAN");
		lblCodiban.setBounds(10, 99, 59, 23);
		frame.getContentPane().add(lblCodiban);

		lblSuma = new JLabel("Suma");
		lblSuma.setBounds(10, 141, 46, 14);
		frame.getContentPane().add(lblSuma);

		lblTipcont = new JLabel("TipCont");
		lblTipcont.setBounds(10, 184, 46, 14);
		frame.getContentPane().add(lblTipcont);

		lblValuta = new JLabel("Valuta");
		lblValuta.setBounds(10, 232, 46, 14);
		frame.getContentPane().add(lblValuta);

		lblronLiraEuro = new JLabel("(RON LIRA EURO)");
		lblronLiraEuro.setBounds(23, 257, 135, 23);
		frame.getContentPane().add(lblronLiraEuro);

		textField_IBAN = new JTextField();
		textField_IBAN.setBounds(67, 100, 86, 20);
		frame.getContentPane().add(textField_IBAN);
		textField_IBAN.setColumns(10);

		textField_suma = new JTextField();
		textField_suma.setBounds(66, 138, 86, 20);
		frame.getContentPane().add(textField_suma);
		textField_suma.setColumns(10);

		textField_tipCont = new JTextField();
		textField_tipCont.setBounds(66, 181, 86, 20);
		frame.getContentPane().add(textField_tipCont);
		textField_tipCont.setColumns(10);

		textField_valuta = new JTextField();
		textField_valuta.setBounds(66, 229, 86, 20);
		frame.getContentPane().add(textField_valuta);
		textField_valuta.setColumns(10);

		btnCreazaCont = new JButton("Creaza cont");
		btnCreazaCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				long iban = Long.parseLong(textField_IBAN.getText());
				String tip = textField_tipCont.getText();
				String valuta = textField_valuta.getText();
				long cnp = Long.parseLong(textField_cnp.getText());
				float sum = Float.parseFloat(textField_suma.getText());
				banc.banca=banc.citesteDinFisier();
				ArrayList<Account> list = new ArrayList<Account>();
				list = banc.banca.get(cnp);
				System.out.println(list.size());
				if (tip.equals("Save")) {
					if (valuta.equals("euro")) {
						Client c = new Client();

						c.setCnp(cnp);
						Account acc = new SavingAccount(sum, Valuta.EURO, iban, c);
						list.add(acc);
						banc.banca.put(cnp, list);
					} else if (valuta.equals("ron")) {
						Client c = new Client();

						c.setCnp(cnp);
						Account acc = new SavingAccount(sum, Valuta.RON, iban, c);
						list.add(acc);
						banc.banca.put(cnp, list);

					} else if (valuta.equals("lira")) {
						Client c = new Client();

						c.setCnp(cnp);
						Account acc = new SavingAccount(sum, Valuta.LIRA, iban, c);
						list.add(acc);
						banc.banca.put(cnp, list);
					}
				}
				if (tip.equals("Spend")) {
					if (valuta.equals("euro")) {
						Client c = new Client();

						c.setCnp(cnp);
						Account acc = new SpendingAccount(sum, Valuta.EURO, iban, c);
						list.add(acc);
						banc.banca.put(cnp, list);

					} else if (valuta.equals("ron")) {
						Client c = new Client();

						c.setCnp(cnp);
						Account acc = new SpendingAccount(sum, Valuta.RON, iban, c);
						list.add(acc);
						banc.banca.put(cnp, list);

					} else if (valuta.equals("lira")) {
						Client c = new Client();

						c.setCnp(cnp);
						Account acc = new SpendingAccount(sum, Valuta.LIRA, iban, c);
						list.add(acc);
						banc.banca.put(cnp, list);
					}
				}
				banc.scrieInfisier();
				//nouTabel("ss");
			}
			
		});
		btnCreazaCont.setBounds(51, 291, 107, 23);
		frame.getContentPane().add(btnCreazaCont);

		lblCnpclient = new JLabel("CNPclient");
		lblCnpclient.setBounds(183, 173, 59, 25);
		frame.getContentPane().add(lblCnpclient);

		textField_cnp = new JTextField();
		textField_cnp.setBounds(237, 175, 86, 20);
		frame.getContentPane().add(textField_cnp);
		textField_cnp.setColumns(10);
		
		btnRetrageBani = new JButton("Retrage Bani");
		btnRetrageBani.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				banc.banca=banc.citesteDinFisier();
				ArrayList<Account> list = new ArrayList<Account>();
				long cnp=Long.parseLong(textField_cnp_retrage.getText());
				list=banc.banca.get(cnp);
				Account acc=list.get(1);
				System.out.println("Vechea suma:"+acc.getSuma());
				if(acc instanceof SavingAccount) {
					acc.retrageBani(Float.parseFloat(textField_suma_retrage.getText())); 
					banc.banca.put(cnp, list);
					banc.scrieInfisier();
					//nouTabel("a mers");
					int x=table.getSelectedRow();
					System.out.println(x);
					Object nouaSuma=acc.getSuma();
					table.getModel().setValueAt(nouaSuma, x, 3);
					System.out.println("Noua suma:"+acc.getSuma());

				}
				if(acc instanceof SpendingAccount) {
					acc.retrageBani(Float.parseFloat(textField_suma_retrage.getText()));
					banc.banca.put(cnp, list);
					banc.scrieInfisier();
					//nouTabel("a mers");
					int x=table.getSelectedRow();
					Object nouaSuma=acc.getSuma();
					table.getModel().setValueAt(nouaSuma, x, 3);
					System.out.println("Noua suma:"+acc.getSuma());

				}
			}
		});
		btnRetrageBani.setBounds(32, 403, 123, 23);
		frame.getContentPane().add(btnRetrageBani);
		
		lblSuma_1 = new JLabel("Suma");
		lblSuma_1.setBounds(10, 337, 46, 14);
		frame.getContentPane().add(lblSuma_1);
		
		lblCnp = new JLabel("CNP");
		lblCnp.setBounds(10, 362, 46, 14);
		frame.getContentPane().add(lblCnp);
		
		textField_suma_retrage = new JTextField();
		textField_suma_retrage.setBounds(67, 334, 86, 20);
		frame.getContentPane().add(textField_suma_retrage);
		textField_suma_retrage.setColumns(10);
		
		textField_cnp_retrage = new JTextField();
		textField_cnp_retrage.setBounds(67, 359, 86, 20);
		frame.getContentPane().add(textField_cnp_retrage);
		textField_cnp_retrage.setColumns(10);
		
		btnDepuneBani = new JButton("Depune bani");
		btnDepuneBani.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				banc.banca=banc.citesteDinFisier();
				ArrayList<Account> list = new ArrayList<Account>();
				long cnp=Long.parseLong(textField_cnp_retrage.getText());
				list=banc.banca.get(cnp);
				int xrow=table.getSelectedRow();
				long iban=(Long) table.getModel().getValueAt(xrow, 2);
				Account acc=acc=list.get(1);
				int index=0;
				for(Account a:list) {
					if(iban==a.getCod()) {
						acc=list.get(index);
						break;
					}
					index++;
				}
				
				System.out.println("Vechea suma:"+acc.getSuma());
				if(acc instanceof SavingAccount) {
					acc.depuneBani(Float.parseFloat(textField_suma_retrage.getText()),Integer.parseInt(textField_luniDobanda.getText())); 
					banc.banca.put(cnp, list);
					banc.scrieInfisier();
					int x=table.getSelectedRow();
					System.out.println(x);
					Object nouaSuma=acc.getSuma();
					table.getModel().setValueAt(nouaSuma, x, 3);
					//nouTabel("a mers");
					System.out.println("Noua suma:"+acc.getSuma());

				}
				if(acc instanceof SpendingAccount) {
					acc.depuneBani(Float.parseFloat(textField_suma_retrage.getText()),Integer.parseInt(textField_luniDobanda.getText()));
					banc.banca.put(cnp, list);
					banc.scrieInfisier();
					//nouTabel("a mers");
					int x=table.getSelectedRow();
					System.out.println(x);
					Object nouaSuma=acc.getSuma();
					table.getModel().setValueAt(nouaSuma, x, 3);
					System.out.println("Noua suma:"+acc.getSuma());

				}
			}
		});
		btnDepuneBani.setBounds(183, 403, 123, 23);
		frame.getContentPane().add(btnDepuneBani);
		
		textField_luniDobanda = new JTextField();
		textField_luniDobanda.setBounds(219, 372, 86, 20);
		frame.getContentPane().add(textField_luniDobanda);
		textField_luniDobanda.setColumns(10);
		
		JLabel lblLuni = new JLabel("Luni");
		lblLuni.setBounds(181, 375, 46, 14);
		frame.getContentPane().add(lblLuni);
		
		lblIban = new JLabel("IBAN");
		lblIban.setBounds(196, 261, 46, 14);
		frame.getContentPane().add(lblIban);
		
		IBANFIELD = new JTextField();
		IBANFIELD.setBounds(237, 258, 86, 20);
		frame.getContentPane().add(IBANFIELD);
		IBANFIELD.setColumns(10);
		
		btnStergecont = new JButton("StergeCont");
		btnStergecont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				banc.citesteDinFisier();
				ArrayList<Account> alist =new ArrayList<Account>();
				Account a;
				Client c=new Client();
				if(textField_stergereType.getText().equals("Saving")) {
					a=new SavingAccount(0, Valuta.RON, Long.parseLong(IBANFIELD.getText()), c);
				}
				else {
					a=new SpendingAccount(0, Valuta.RON, Long.parseLong(IBANFIELD.getText()), c);
				}
				long clientCNP= Long.parseLong(cnp_stergeCont.getText());
				banc.stergeCont(a, clientCNP);
				banc.scrieInfisier();
			}
		});
		btnStergecont.setBounds(237, 328, 89, 23);
		frame.getContentPane().add(btnStergecont);
		
		lblCnpclient_1 = new JLabel("CnpClient");
		lblCnpclient_1.setBounds(181, 291, 61, 14);
		frame.getContentPane().add(lblCnpclient_1);
		
		cnp_stergeCont = new JTextField();
		cnp_stergeCont.setBounds(233, 292, 86, 20);
		frame.getContentPane().add(cnp_stergeCont);
		cnp_stergeCont.setColumns(10);
		
		textField_stergereType = new JTextField();
		textField_stergereType.setBounds(237, 229, 86, 20);
		frame.getContentPane().add(textField_stergereType);
		textField_stergereType.setColumns(10);
		
		lblType = new JLabel("Type");
		lblType.setBounds(181, 232, 46, 14);
		frame.getContentPane().add(lblType);
	}

	public void nouTabel(String flag) {
		Hashtable<Long, ArrayList<Account>> ht = new Hashtable<Long, ArrayList<Account>>();
		ht = banc.citesteDinFisier();
		DefaultTableModel modeltable = (DefaultTableModel) table.getModel();
		Set<Long> set;
		set = ht.keySet();
		ArrayList<Account> toateConturile = new ArrayList<Account>();

		for (Long l : set) {
			ArrayList<Account> alist = new ArrayList<Account>();
			alist = ht.get(l);
			for (Account a : alist) {
				toateConturile.add(a);
			}
		}

		for (int i = 0; i < toateConturile.size(); i++) {
			System.out.println();
			modeltable.addRow(new Object[] { toateConturile.get(i).getClient().getPrenume(),
					toateConturile.get(i).getClient().getCnp(), toateConturile.get(i).getCod(),
					toateConturile.get(i).getSuma(), toateConturile.get(i).getType(),
					toateConturile.get(i).getValuta() });

		}
		table = new JTable(new DefaultTableModel(data, column));
		table.setModel(modeltable);
	}
}
