package entitati;
import java.io.Serializable;
public class SpendingAccount extends Account implements Serializable{
		

	/**
	 * 
	 */
	private static final long serialVersionUID = -8987421867425935532L;
	private final float comision =(float) 0.03;

	public SpendingAccount(float suma, Valuta valuta, long cod, Client client) {
		super("Spending", suma, valuta, cod, client);

	}

	public boolean retrageBani(float suma) {
		float sumaCurenta=this.getSuma();
		float sumaFinala=sumaCurenta-suma- suma*comision;
		this.setSuma(sumaFinala);
		
		return true;
	}

	public boolean depuneBani(float suma,double nrLuni) {
		float sumacurent=this.getSuma();
		this.setSuma(suma+sumacurent);
		System.out.println("Am schimbat suma in:"+ this.getSuma());
		return true;
		
	}
	
	
	
}
