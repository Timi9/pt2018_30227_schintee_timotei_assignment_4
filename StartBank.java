package start;

import java.util.ArrayList;
import java.util.Hashtable;

import bank.Banc;
import entitati.Account;
import entitati.Client;
import entitati.SavingAccount;
import entitati.Valuta;
import presentation.OptiuniFerestre;

public class StartBank {
	private static final long serialVersionUID = 1L;


	public static void main(String[] args) {
		Banc banc= new Banc();
		Client c = new Client();
		c.setCnp(1234);
		c.setPrenume("Geo");
		Account a= new SavingAccount(23, Valuta.EURO, 2345, c);
		
		//banc.adaugaClient(c);
		//banc.scrieInfisier();
		//banc.adaugaCont(a, c.getCnp());
		Hashtable<Long, ArrayList<Account>> ht;
		//ht=banc.citesteDinFisier();
		//System.out.println(ht.containsKey(c.getCnp()));
		//System.out.println((ht.get(c.getCnp()).get(0).getValuta()));
		new OptiuniFerestre();
	}

}
