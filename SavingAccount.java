package entitati;
import java.io.Serializable;
public class SavingAccount extends Account implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5821589119295295334L;
	/**
	 * 
	 */

	private final float dobanda= (float) 0.05;
	

	public SavingAccount(float sum, Valuta valuta, long cod, Client client) {
		super("Saving", sum, valuta, cod, client);

	}
	
	public boolean retrageBani(float suma) {
		float sumaCurenta = this.getSuma();
		float sumaNoua= sumaCurenta-suma;
		this.setSuma(sumaNoua);
		return true;
	}

	public boolean depuneBani(float suma, double nrLuni) {		
		float sumaCurenta=this.getSuma();
		float sumaDobanda= (float) (suma*nrLuni*dobanda);
		float sumaNoua=suma+sumaCurenta+ sumaDobanda;
		this.setSuma(sumaNoua);
		return true;
	}
	
public String numeleClientuluiCeDetineContul() {
		String numePrenume= this.getClient().getNume() + this.getClient().getPrenume();
		return numePrenume;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SavingAccount [toString()=" + super.toString() + "]";
	}

	/**
	 * @return the dobanda
	 */
	public float getDobanda() {
		return dobanda;
	}

	
	
}
