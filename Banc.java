package bank;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;

import entitati.*;

public class Banc implements BankProc,Serializable {
	private static final long serialVersionUID = 1L;


	public Hashtable<Long, ArrayList<Account>> banca = new Hashtable<Long, ArrayList<Account>>();
	

	public boolean adaugaCont(Account account, long clientCNP) {
		try {
			ArrayList<Account> list = new ArrayList<Account>(); //creez o noua lista
			list = banca.get(clientCNP); //ii atribui listei create lista de conturi a cientului cu cnp=clientCNP

			list.add(account); //in lista de conturi de mai sus adaug contul account dat ca parametru

			banca.put(clientCNP, list);
			System.out.println("Am bagat un cont nou!");
			return true;
		} catch (Exception e) {
			System.out.println("Error! Nu am putut baga cont nou!!");
			e.getMessage();
			e.printStackTrace();
		}
		return false;
	}


	public boolean stergeCont(Account account, long clientCNP) {
		try {
			ArrayList<Account> list = new ArrayList<Account>();
			list = banca.get(clientCNP);
			int index=0;
			for(Account a:list) { //parcurgem lista de conturi
				if(account.getCod()==a.getCod()) {  //daca gasim contul cu Codul accunt.getCo() stergem contul deoarece l am gasit
					list.remove(index);
				}
				index++;
			}			
			banca.put(clientCNP, list);
			System.out.println("Am sters un client!");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
		return false;

	}
	public boolean editClient(Client c) {
		this.banca=citesteDinFisier();
		ArrayList<Account> finallist = new ArrayList<Account>();
		
		finallist=banca.get(c.getCnp());
		finallist.get(0).setClient(c);
		banca.put(c.getCnp(), finallist);
		return true;
	}
	
	public boolean editClienta(Client c) {
		this.banca=citesteDinFisier();
		ArrayList<Account> list = new ArrayList<Account>();
		list = banca.get(c.getCnp()); //list e lista de conturi a clientului
		
		Account cont = list.get(0); //primul cont al persoanei
		Client client = cont.getClient();//da mi clientul de la contul 0, adica clientul vechi 
		client.setEmail(c.getEmail()); //ii actualizam email nume prenume deoarece cnp e acelasi
		client.setNume(c.getNume());
		client.setPrenume(c.getPrenume());
		
		for (Account a: list)
		{
			a.setClient(client); //actuaizam ca fiecare cont al persoanei sa aiba clientul modificat/actualizat
		}
				
			
		return true;
	}
	
	public boolean isAccount(String key){
		
		if(banca.containsKey(Long.parseLong(key)))
			return true;
		else
			return false;
	}
	
	public int totalNrOfAccounts() {
		this.banca=citesteDinFisier();
		ArrayList<Account> finallist = new ArrayList<Account>();
		Set<Long> mySet= banca.keySet();
		for(Long x: mySet) {
			ArrayList<Account> aux = new ArrayList<Account>();
			aux=banca.get(x);
			for(Account acc: aux) {
				finallist.add(acc);
			}
		}
		return finallist.size();
	}
	
	public Collection<ArrayList<Account>> getAccounts(){	
		return this.banca.values();
	}


	public void editeazaCont(Account account, long clientCNP) {
		ArrayList<Account> list = new ArrayList<Account>();
		list = banca.get(clientCNP);
		for(Account a:list) { //parcurgem lista de conturi
			if(account.getCod()==a.getCod()) {  //daca gasim contul cu Codul accunt.getCod() atunci il modificams
				a.setClient(account.getClient());
				a.setSuma(account.getSuma());
				a.setType(account.getType());
				a.setValuta(account.getValuta());
			}
			
		}
				
		
	}
	
	public boolean adaugaClient(Client c) {
		try {
		SavingAccount acc = new SavingAccount(0, Valuta.RON, 9999, c);
		
		ArrayList<Account> list = new ArrayList<Account>();
		list.add(acc); //in lista creata punem contul creat mai sus
		banca.put(c.getCnp(), list); //in banca punem contul si persoana
		System.err.println("Nou client!!");
		return true;
		}
		catch (Exception e) {
			System.out.println("Nu merge bagat client nou");
			e.printStackTrace();
		}
		return false;
	}


	public void scrieInfisier() {
		try {
			FileOutputStream fileOut = new FileOutputStream("inout.ser");
			//FileOutputStream fileOut = new FileOutputStream("inout",true); pentru a nu mai suprascrie.dar tot nu merge
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this.banca);
			out.close();
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Nu pot scrie in fisier");
		}

	}


	public Hashtable<Long, ArrayList<Account>> citesteDinFisier() {
		Hashtable<Long, ArrayList<Account>> banc = new Hashtable<Long, ArrayList<Account>>();
		try {			
			FileInputStream fileIn = new FileInputStream("inout");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			banc = (Hashtable<Long, ArrayList<Account>>) in.readObject();
			in.close();
			fileIn.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Nu pot citii din fisier");
		}
		return banc;
	}

}
