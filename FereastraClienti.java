package presentation;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import entitati.*;
import bank.Banc;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FereastraClienti {

	private JFrame frame;
	private Object[][] data = { { "Ra", "Ionel","Test@yahoo.com","19873473", "33","44", "Spending", "EURO" } };
	
	private String[] column = { "NumeClient","Prenume","Email","CNP","CodIBAN", "Suma", "TipCont", "Valuta" };
	JLabel lblTotiClientii = new JLabel("Toti Clientii");
	
	JScrollPane paneTabel = new JScrollPane();
	private JTable tabelClienti;
	private Banc banc = new Banc();
	private JLabel numeLabel;
	private JLabel lblPrenume;
	private JLabel lblEmail;
	private JLabel lblCnp;
	private JTextField numeInput;
	private JTextField prenumeInput;
	private JTextField emailInput;
	private JTextField cnpInput;
	/** Email validation */
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	private JTextField textFieldDelete;
	private JButton btnEditeaza;

	public FereastraClienti() {

		numeInput = new JTextField();
		prenumeInput = new JTextField();
		emailInput = new JTextField();
		cnpInput = new JTextField();
		numeLabel = new JLabel("Nume");
		lblPrenume = new JLabel("Prenume");
		lblEmail = new JLabel("Email");
		lblCnp = new JLabel("CNP");
		initialize();
		frame.setVisible(true);
	}

	private void initialize() {

		frame = new JFrame();
		tabelClienti = new JTable(new DefaultTableModel(data, column));

		DefaultTableModel modeltable = (DefaultTableModel) tabelClienti.getModel();
		frame.getContentPane().setBackground(UIManager.getColor("CheckBox.light"));
		frame.getContentPane().setLayout(null);

		paneTabel.setBounds(23, 29, 494, 374);
		frame.getContentPane().add(paneTabel);

		paneTabel.setColumnHeaderView(tabelClienti);
		
		paneTabel.setViewportView(tabelClienti);

		lblTotiClientii.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblTotiClientii.setBounds(136, 11, 144, 14);
		frame.getContentPane().add(lblTotiClientii);

		numeLabel.setBounds(527, 48, 61, 25);
		frame.getContentPane().add(numeLabel);

		lblPrenume.setBounds(527, 84, 75, 21);
		frame.getContentPane().add(lblPrenume);

		lblEmail.setBounds(527, 116, 75, 25);
		frame.getContentPane().add(lblEmail);

		lblCnp.setBounds(539, 152, 61, 20);
		frame.getContentPane().add(lblCnp);

		numeInput.setBounds(598, 50, 86, 20);
		frame.getContentPane().add(numeInput);
		numeInput.setColumns(10);

		prenumeInput.setBounds(598, 84, 86, 20);
		frame.getContentPane().add(prenumeInput);
		prenumeInput.setColumns(10);

		emailInput.setBounds(598, 118, 86, 20);
		frame.getContentPane().add(emailInput);
		emailInput.setColumns(10);

		cnpInput.setBounds(598, 152, 86, 20);
		frame.getContentPane().add(cnpInput);
		cnpInput.setColumns(10);

		JButton btnAdaugaClientNou = new JButton("Adauga client nou!");
		btnAdaugaClientNou.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nume = numeInput.getText();
				String prenume = prenumeInput.getText();
				long cnp = Long.parseLong(cnpInput.getText());
				String email = emailInput.getText();
				if (validate(email) == false) {
					JOptionPane.showMessageDialog(frame, "Adresa introdusa nu este un email valid!");
				} else {
					banc.citesteDinFisier();
					Client c = new Client(nume, prenume, cnp, email);
					ArrayList<Account> auxConturi = new ArrayList<Account>();
					Account acc = new SavingAccount(0, Valuta.RON, 99999, c);
					Hashtable<Long, ArrayList<Account>> ht = new Hashtable<Long, ArrayList<Account>>();
					banc.adaugaClient(c);
					banc.scrieInfisier();
					//nouTabel("merge");

				}
			}
		});
		btnAdaugaClientNou.setBounds(550, 201, 134, 37);
		frame.getContentPane().add(btnAdaugaClientNou);
		
		JButton stergebuton = new JButton("Sterge");
		stergebuton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				banc.citesteDinFisier();
				int row= tabelClienti.getSelectedRow();				
				long cnp=Long.parseLong(textFieldDelete.getText());
				banc.banca.remove(cnp);
				banc.scrieInfisier();
			}
		});
		stergebuton.setBounds(550, 286, 134, 23);
		frame.getContentPane().add(stergebuton);
		
		textFieldDelete = new JTextField();
		textFieldDelete.setBounds(585, 249, 86, 20);
		frame.getContentPane().add(textFieldDelete);
		textFieldDelete.setColumns(10);
		
		JLabel lblSterge = new JLabel("Sterge");
		lblSterge.setBounds(529, 249, 46, 14);
		frame.getContentPane().add(lblSterge);
		
		btnEditeaza = new JButton("Editeaza");
		btnEditeaza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Account> toateConturile = new ArrayList<Account>();
				
				String nume = numeInput.getText();
				String prenume = prenumeInput.getText();
				long cnp = Long.parseLong(cnpInput.getText());
				String email = emailInput.getText();
				banc.citesteDinFisier();
				toateConturile=banc.banca.get(cnp);
				Client nou = new Client();
				nou.setNume(nume);
				nou.setEmail(email);
				nou.setPrenume(prenume);
				nou.setNume(prenume);
				nou.setCnp(cnp);
				banc.editClient(nou);
				banc.scrieInfisier();
			}
		});
		btnEditeaza.setBounds(585, 363, 99, 40);
		frame.getContentPane().add(btnEditeaza);
		frame.setTitle("Administrare Clienti");
		frame.setBounds(100, 100, 768, 470);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Hashtable<Long, ArrayList<Account>> ht = new Hashtable<Long, ArrayList<Account>>();
		ht = banc.citesteDinFisier();

		Set<Long> set;
		set = ht.keySet();
		ArrayList<Account> toateConturile = new ArrayList<Account>();

		for (Long l : set) {
			ArrayList<Account> alist = new ArrayList<Account>();
			alist = ht.get(l);
			for (Account a : alist) {
				toateConturile.add(a);
			}
		}

		for (int i = 0; i < toateConturile.size(); i++) {
			System.out.println();
			modeltable.addRow(new Object[] { toateConturile.get(i).getClient().getPrenume(), toateConturile.get(i).getClient().getNume(), toateConturile.get(i).getClient().getEmail(), toateConturile.get(i).getClient().getCnp(),
					toateConturile.get(i).getCod(), toateConturile.get(i).getSuma(), toateConturile.get(i).getType(),
					toateConturile.get(i).getValuta() });

		}

	}

	public void nouTabel(String flag) {
		Hashtable<Long, ArrayList<Account>> ht = new Hashtable<Long, ArrayList<Account>>();
		ht = banc.citesteDinFisier();
		DefaultTableModel modeltable = (DefaultTableModel) tabelClienti.getModel();
		Set<Long> set;
		set = ht.keySet();
		ArrayList<Account> toateConturile = new ArrayList<Account>();

		for (Long l : set) {
			ArrayList<Account> alist = new ArrayList<Account>();
			alist = ht.get(l);
			for (Account a : alist) {
				toateConturile.add(a);
			}
		}

		for (int i = 0; i < toateConturile.size(); i++) {
			System.out.println();
			modeltable.addRow(new Object[] { toateConturile.get(i).getClient().getPrenume(), toateConturile.get(i).getClient().getNume(), toateConturile.get(i).getClient().getEmail(),
					toateConturile.get(i).getCod(), toateConturile.get(i).getSuma(), toateConturile.get(i).getType(),
					toateConturile.get(i).getValuta() });

		}

	}

	public static boolean validate(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	public Object[][] createModel() {
		Banc bank = new Banc();
		Hashtable<Long, ArrayList<Account>> ht = new Hashtable<Long, ArrayList<Account>>();
		ht = bank.citesteDinFisier();
		int x;
		Iterator<ArrayList<Account>> iterator = bank.getAccounts().iterator();
		x = bank.totalNrOfAccounts();
		ArrayList<Account> list = new ArrayList<Account>();
		data = new Object[x][7];
		System.out.println(x);
		int i = 0;
		while (iterator.hasNext()) {

			list = iterator.next();
			System.out.println("Size de lista e: " + list.size());
			for (int j = 0; j < list.size(); j++) {
				data[i][0] = list.get(j).getClient().getNume();
				data[i][1] = list.get(j).getClient().getPrenume();
				data[i][2] = list.get(j).getClient().getEmail();
				System.out.println(list.get(j).getClient().getNume());
				data[i][3] = list.get(j).getCod();
				data[i][4] = list.get(j).getSuma();
				data[i][5] = list.get(j).getType();
				data[i][6] = list.get(j).getValuta();
				i++;
			}
		}

		return data;
	}
}
