package presentation;


import javax.swing.JFrame;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

public class OptiuniFerestre {

	private JFrame frame;
	JLabel labelSus = new JLabel("Bine ati venit !");
	JRadioButton butonClienti = new JRadioButton("Procesare Clienti");
	JRadioButton butonConturi = new JRadioButton("Procesare Conturi");
	JButton btnProceseaza = new JButton("Proceseaza");

	public OptiuniFerestre() {
	
		

		frame = new JFrame();
		frame.getContentPane().setBackground(UIManager.getColor("CheckBox.darkShadow"));
		frame.setTitle("Administrare Optiuni");
		frame.setBounds(100, 100, 636, 433);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		labelSus.setBounds(242, 6, 175, 33);
		labelSus.setForeground(Color.RED);
		labelSus.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		frame.getContentPane().add(labelSus);
		butonClienti.setForeground(Color.RED);
		butonClienti.setFont(new Font("Times New Roman", Font.BOLD, 14));
		

		butonClienti.setBounds(56, 82, 157, 47);
		frame.getContentPane().add(butonClienti);
		butonConturi.setForeground(Color.RED);
		butonConturi.setFont(new Font("Times New Roman", Font.BOLD, 14));
		
	
		butonConturi.setBounds(392, 82, 139, 33);
		frame.getContentPane().add(butonConturi);
		btnProceseaza.setBackground(Color.LIGHT_GRAY);
		btnProceseaza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(butonClienti.isSelected()==true) {
					new FereastraClienti();
				}
				else if(butonConturi.isSelected()==true) {
					new FereastraConturi();
				}
				else {
					JOptionPane.showMessageDialog(frame, "Selecteaza una din optiuni si apoi apasa butonul");
				}
				
			}
			
		});
		

		btnProceseaza.setBounds(211, 136, 175, 44);
		btnProceseaza.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 12));
		frame.getContentPane().add(btnProceseaza);
		
		JLabel lblSelecteazaOOptiune = new JLabel("Selecteaza o optiune si apoi apasa butonul !");
		lblSelecteazaOOptiune.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblSelecteazaOOptiune.setBounds(143, 260, 377, 47);
		frame.getContentPane().add(lblSelecteazaOOptiune);
		frame.setVisible(true);
	}
	
	public void apasare(ActionEvent arg0) {
	}
}
